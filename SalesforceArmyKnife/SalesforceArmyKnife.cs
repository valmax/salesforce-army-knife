﻿//
//  SalesforceArmyKnife.cs
//
//  Author:
//       Matías Rodríguez Llanos <mrllanos@gmail.com>
//
//  Copyright (c) 2017 Valmax IT S.A.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using valmaxit.salesforce.bulk;
using valmaxit.salesforce.process;
using static valmaxit.salesforce.bulk.SalesforceMessages;

namespace valmaxit.salesforce {
    public class SalesforceArmyKnife {
        private ConsoleOptions _objOptions = null;

        public SalesforceArmyKnife (string[] arrArguments) {
            initializeClass (arrArguments);
            processArguments ();
        }

        private void processArguments () {
            SalesforceConnectorBase objSalesforce = null;
            Credentials objCredentials = null;
            PayloadRequest objProcess = null;

			objCredentials = retrieveCredentials (_objOptions);

            switch (_objOptions.Operation.Value) {
                case ConsoleOptions.OperationOptions.Insert:
                    objSalesforce = new SalesforceConnectorInsert (objCredentials);
                    ((SalesforceConnectorInsert)objSalesforce).createJob (_objOptions.sObject.Value);
                    objProcess = new PayloadRequest (_objOptions.File.Value);

        			((SalesforceConnectorInsert)objSalesforce).addBatchToJob(objProcess);
				break;
                case ConsoleOptions.OperationOptions.Update:
    				objSalesforce = new SalesforceConnectorUpdate (objCredentials);
    				((SalesforceConnectorUpdate)objSalesforce).createJob (_objOptions.sObject.Value);
    				objProcess = new PayloadRequest (_objOptions.File.Value);

    				((SalesforceConnectorUpdate)objSalesforce).addBatchToJob (objProcess);
    				break;
                case ConsoleOptions.OperationOptions.Delete:
    				objSalesforce = new SalesforceConnectorDelete (objCredentials);
    				((SalesforceConnectorDelete)objSalesforce).createJob (_objOptions.sObject.Value);
    				objProcess = new PayloadRequest (_objOptions.File.Value);

    				((SalesforceConnectorDelete)objSalesforce).addBatchToJob (objProcess);
    				break;
			case ConsoleOptions.OperationOptions.Purge:
                objSalesforce = new SalesforceConnectorPurge (objCredentials);
				((SalesforceConnectorPurge)objSalesforce).createJob (_objOptions.sObject.Value);
				objProcess = new PayloadRequest (_objOptions.File.Value);

				((SalesforceConnectorPurge)objSalesforce).addBatchToJob (objProcess);
				break;
			default:
                    break;
            }
        }

        private void initializeClass (string[] arrArguments) {
            configureLogger ();

            _objOptions = new ConsoleOptions ();
            _objOptions.Parse (arrArguments);
        }

        private Credentials retrieveCredentials (ConsoleOptions objOptions) {
            Credentials objCredentials = null;

            objCredentials = new Credentials ();
            objCredentials.Username = objOptions.Username.Value;
            objCredentials.Password = objOptions.Password.Value;
            objCredentials.Token = objOptions.Token.Value;
            objCredentials.ClientID = objOptions.ClientId.Value;
            objCredentials.ClientSecret = objOptions.ClientSecret.Value;
            objCredentials.IsSandbox = (objOptions.Instance.Value == ConsoleOptions.InstanceOptions.Sandbox);

            return objCredentials;
        }

        private void configureLogger () {
            log4net.Config.BasicConfigurator.Configure ();
        }
    }
}

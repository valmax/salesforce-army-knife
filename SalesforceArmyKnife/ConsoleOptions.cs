﻿//
//  ConsoleOptions.cs
//
//  Author:
//       Matías Rodríguez Llanos <mrllanos@gmail.com>
//
//  Copyright (c) 2017 Valmax IT S.A.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using OpenSoftware.OptionParsing;

namespace valmaxit.salesforce {
    public class ConsoleOptions : OptionParser {
		public enum OperationOptions {
			Insert,
			Update,
			Upsert,
            Delete,
            Query,
            Purge
		};

		public enum InstanceOptions {
			Sandbox,
			Production
		};

        public override string Name => "Salesforce Army Knife";
		public override string Description => "This application can insert, update, upsert, delete and query salesforce data.";

		[Option(Description = "Salesforce username", Name = "--username", ShortName = "-u", IsRequired = true)]
        public StringOption Username { get; set; }

		[Option(Description = "Salesforce password", Name = "--password", ShortName = "-p", IsRequired = true)]
        public StringOption Password { get; set; }

		[Option(Description = "Salesforce token", Name = "--token", ShortName = "-t", IsRequired = true)]
        public StringOption Token { get; set; }

		[Option(Description = "Salesforce Connected App Client Id", Name = "--clientid", ShortName = "-c", IsRequired = true)]
		public StringOption ClientId { get; set; }

		[Option(Description = "Salesforce Connected App Client Secret", Name = "--clientsecret", ShortName = "-s", IsRequired = true)]
		public StringOption ClientSecret { get; set; }

        [Option(Description = "Salesforce Instance [Production | Sandbox]", Name = "--instance", ShortName = "-i")]
        public EnumOption<InstanceOptions> Instance { get; set; }

		[Option(Description = "Salesforce operation", Name = "--operation", ShortName = "-o", IsRequired = true)]
        public EnumOption<OperationOptions> Operation { get; set; }

        [Option(Description = "Salesforce sObject", Name = "--sobject", ShortName = "-so", IsRequired = true)]
        public StringOption sObject { get; set; }

		[Option(Description = "File to process", Name = "--file", ShortName = "-f")]
		public FileOption File { get; set; }
    }
}

﻿//  SalesforceArmyKnife.cs
//
//  Author:
//       Matías Rodríguez Llanos <mrllanos@gmail.com>
//
//  Copyright (c) 2017 Valmax IT S.A.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

/****
 * mono SalesforceArmyKnife.exe -o insert -u mmmmm@mmmm.com.ar -p 1111111111 -t 11111111111111111 -c 1111111111111111111111111 -s 11111111111111111111111
 * */
using System;

namespace valmaxit.salesforce {
    public class MainClass {
        public static void Main (string [] arrArguments) {
            SalesforceArmyKnife objArmyKnife = null;

            try {
                objArmyKnife = new SalesforceArmyKnife(arrArguments);

            } catch (Exception Ex) {
                Console.WriteLine("Message : " + Ex.Message);
                Console.WriteLine("StackTrace : " + Ex.StackTrace);
            }
        }
    }
}

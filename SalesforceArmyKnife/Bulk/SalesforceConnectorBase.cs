﻿//
//  SalesforceConnectorBase.cs
//
//  Author:
//       Matías Rodríguez Llanos <mrllanos@gmail.com>
//
//  Copyright (c) 2017 Valmax IT S.A.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using static valmaxit.salesforce.bulk.SalesforceMessages;

namespace valmaxit.salesforce.bulk {
    public abstract class SalesforceConnectorBase {
        private static readonly log4net.ILog _objLogger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string LOGIN_URL = "https://{0}.salesforce.com/services/Soap/u/{1}.0";

        public enum Operations { Insert, Update, Upsert, Delete, hardDelete }
		protected Credentials _objCredentials = null;
        protected LoginResponse _objLoginResponse = null;
        protected JobResponse _objJobResponse = null;
        protected List<BatchResponse> _lstBatchs = null;
        protected int API_VERSION { get { return 41; } }

        protected SalesforceConnectorBase(Credentials objCredentials) {
            _objCredentials = objCredentials;
            if(_objLoginResponse == null)  {
                this.login ();
            }
        }

        protected bool login() {
            string strResponse = null;
            RESTRequest objRequest = null;
            bool boolIsLoggedIn = false;

			_objLogger.DebugFormat("login () ->");

            if (_objCredentials.isValid()) {
                objRequest = new RESTRequest();
                objRequest.EndPoint = string.Format(LOGIN_URL, (_objCredentials.IsSandbox)?"test":"login", this.API_VERSION );
                objRequest.Method = RESTRequest.HttpVerb.POST;
                objRequest.PostData = _objCredentials.toRequest();
                objRequest.ContentType = "text/xml; charset=UTF-8";
                objRequest.Headers.Add("SOAPAction", "login");

				strResponse = makeRequest(objRequest);
                _objLoginResponse = LoginResponse.parseResponse(strResponse);
				_objLogger.DebugFormat("SessionId ({0})", _objLoginResponse.SessionId);
				_objLogger.DebugFormat("ServerURL ({0})", _objLoginResponse.ServerUrl);
                _objLogger.DebugFormat("apiURL ({0})", _objLoginResponse.apiURL);

				if (_objLoginResponse != null && !String.IsNullOrEmpty(_objLoginResponse.SessionId)) {
                    boolIsLoggedIn = true;
                }
            } else {
                _objLogger.Debug("Not valid configuration");
            }

			
            _objLogger.DebugFormat("login ({0}) <-", boolIsLoggedIn);

			return boolIsLoggedIn;
        }
       
        protected bool createJOB(JobRequest objBatchRequest) {
            RESTRequest objRequest = null;
            String strEndPoint = null;
            String strJOBResponse = null;
            bool boolJobCreated = false;

            _objLogger.DebugFormat("createJOB (objBatchRequest : {0}) ->", objBatchRequest.toJSON());

            objRequest = new RESTRequest();

            strEndPoint = String.Format("{0}/services/async/{1}.0/job", this._objLoginResponse.InstanceUrl, API_VERSION);

            _objLogger.Debug("strEndPoint : " + strEndPoint);
            _objLogger.Debug("JSON : " + objBatchRequest.toXML());

            objRequest.EndPoint = strEndPoint;
            objRequest.Method = RESTRequest.HttpVerb.POST;

            objRequest.PostData = objBatchRequest.toXML();

            objRequest.ContentType = "text/xml; charset=UTF-8";
            objRequest.Headers.Add("X-SFDC-Session", "Bearer " + _objLoginResponse.SessionId);
            objRequest.Headers.Add("SOAPAction", objBatchRequest.Operation.ToLower());
            //objRequest.Headers.Add("Accept-Encoding", "gzip");

            _objLogger.Debug("objRequest : " + objRequest);

            strJOBResponse = makeRequest(objRequest);

            _objJobResponse = JobResponse.parseResponse(strJOBResponse);

            boolJobCreated =  !String.IsNullOrEmpty(_objJobResponse.id);
            _objLogger.DebugFormat("createJOB (" + boolJobCreated + ") <-");

            return boolJobCreated;
        }

        protected bool addBatchToJob(JobResponse objJob, string[] arrBatchLines) {
            string strEndPoint = null;
            RESTRequest objRequest = null;
            string strBatchResponse = null;
            bool boolBatchAdded = false;
            BatchResponse objBatchResponse = null;

            _objLogger.DebugFormat("addBatchToJob (objJob : {0} - objBatch : ) ->", objJob);
            
            objRequest = new RESTRequest();

            strEndPoint = String.Format("{0}/services/async/{1}.0/job/{2}/batch", this._objLoginResponse.InstanceUrl, API_VERSION, objJob.id);

            _objLogger.Debug("strEndPoint : " + strEndPoint);

			objRequest.EndPoint = strEndPoint;
			objRequest.Method = RESTRequest.HttpVerb.POST;
            objRequest.PostData = string.Join ("\n",arrBatchLines);

			objRequest.ContentType = "text/csv; charset=UTF-8";
			objRequest.Headers.Add("X-SFDC-Session", "Bearer " + _objLoginResponse.SessionId);
			objRequest.Headers.Add("SOAPAction", objJob.operation.ToLower());

			strBatchResponse = makeRequest(objRequest);

			objBatchResponse = BatchResponse.parseResponse(strBatchResponse);
			boolBatchAdded = !String.IsNullOrEmpty(objBatchResponse.id);

			if (_lstBatchs == null) {
				_lstBatchs = new List<BatchResponse>();
			}

			_lstBatchs.Add(objBatchResponse);
            _objLogger.DebugFormat("addBatchToJob (" + boolBatchAdded + ") <-");

			return boolBatchAdded;
		}

        protected void closeJob(JobResponse objJob) {
			string strEndPoint = null;
			RESTRequest objRequest = null;
			string strBatchResponse = null;
		
            _objLogger.DebugFormat ("closeJob (objJob : {0}) ->", objJob);

			objRequest = new RESTRequest ();

			strEndPoint = String.Format ("{0}/services/async/{1}.0/job/{2}", this._objLoginResponse.InstanceUrl, API_VERSION, objJob.id);

			_objLogger.Debug ("strEndPoint : " + strEndPoint);

			objRequest.EndPoint = strEndPoint;
			objRequest.Method = RESTRequest.HttpVerb.POST;
			objRequest.PostData = objJob.toCloseJobRequest();

			objRequest.ContentType = "text/xml; charset=UTF-8";
			objRequest.Headers.Add ("X-SFDC-Session", "Bearer " + _objLoginResponse.SessionId);
			objRequest.Headers.Add ("SOAPAction", "closeJOB");

			strBatchResponse = makeRequest (objRequest);

			_objLogger.DebugFormat ("closeJob () <-");
        }

		protected string makeRequest(RESTRequest objRESTRequest) {
            HttpWebRequest objRequest = null;
            byte[] arrBytes = null;
            string strResponseValue = null;
            string strErrorMessage = null;

            _objLogger.DebugFormat("makeRequest ( objRESTRequest : {0} ) ->", objRESTRequest);
            
            _objLogger.DebugFormat("makeRequest - [POST DATA : {0}]", objRESTRequest.PostData);

            objRESTRequest.UrlParamters = string.IsNullOrEmpty(objRESTRequest.UrlParamters) ? "" : objRESTRequest.UrlParamters;

            objRequest = (HttpWebRequest)WebRequest.Create(objRESTRequest.EndPoint + objRESTRequest.UrlParamters);
            objRequest.Method = objRESTRequest.Method.ToString();
            objRequest.ContentLength = 0;
            objRequest.ContentType = objRESTRequest.ContentType;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;

            if (objRESTRequest.Headers != null && objRESTRequest.Headers.Count > 0) {
                foreach (KeyValuePair<string, string> objHeader in objRESTRequest.Headers) {
                    objRequest.Headers.Add(objHeader.Key, objHeader.Value);
                }
            }

            if (!string.IsNullOrEmpty(objRESTRequest.PostData) && objRESTRequest.Method == RESTRequest.HttpVerb.POST) {
                arrBytes = Encoding.GetEncoding("iso-8859-1").GetBytes(objRESTRequest.PostData);
                objRequest.ContentLength = arrBytes.Length;

                using (var objRequestStream = objRequest.GetRequestStream()) {
                    objRequestStream.Write(arrBytes, 0, arrBytes.Length);
                }
            }

            try {
                
                using (var objResponse = (HttpWebResponse)objRequest.GetResponse()) {
                    strResponseValue = string.Empty;
                    
                    if (objResponse.StatusCode == HttpStatusCode.Created || objResponse.StatusCode == HttpStatusCode.OK) {
                        using (var objResponseStream = objResponse.GetResponseStream()) {
                            strResponseValue = retrieveResponse(objResponseStream);
                        }
                    } else {
                        using (var objResponseStream = objResponse.GetResponseStream()) {
                            retrieveResponse(objResponseStream);
                        }

                        strErrorMessage = String.Format("Request failed. Received HTTP {0}", objResponse.StatusCode);

                        throw new ApplicationException(strErrorMessage);
                    }
                }
            } catch (WebException Ex) {
                using (var objResponseStream = Ex.Response.GetResponseStream()) {
                    retrieveResponse(objResponseStream);
                }
                _objLogger.ErrorFormat("EX : {0}", Ex.Data);
            }

            _objLogger.DebugFormat("makeRequest - [{0}] <-", strResponseValue);

			return strResponseValue;
        }

        private static string retrieveResponse(Stream objStream) {
            string strResponseValue = null;
			
            _objLogger.DebugFormat("retrieveResponse ( objStream : {0} ) ->", objStream);

			if (objStream != null) {
                using (var objResponseReader = new StreamReader(objStream)) {
                    strResponseValue = objResponseReader.ReadToEnd();
                }
            }

            _objLogger.DebugFormat("Response Stream : {0} <-", strResponseValue);

            return strResponseValue;
        }
    }
}

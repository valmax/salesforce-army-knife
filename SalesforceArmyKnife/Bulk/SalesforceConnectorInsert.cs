﻿//
//  SalesforceConnectorInsert.cs
//
//  Author:
//       Matías Rodríguez Llanos <mrllanos@gmail.com>
//
//  Copyright (c) 2017 Valmax IT S.A.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using valmaxit.salesforce.process;
using static valmaxit.salesforce.bulk.SalesforceMessages;

namespace valmaxit.salesforce.bulk {
    public sealed class SalesforceConnectorInsert : SalesforceConnectorBase{
        public SalesforceConnectorInsert(Credentials objCredentials) 
            : base(objCredentials) {
        }

        public bool createJob(string strSObject) {
			JobRequest objBatchRequest = null;
            bool boolJobCreated = false;

			objBatchRequest = new JobRequest();
			objBatchRequest.ContentType = "CSV";
            objBatchRequest.Concurrency = JobRequest.EConcurrency.Parallel;
            objBatchRequest.Operation = Operations.Insert.ToString ().ToLower ();
			objBatchRequest.sObject = strSObject;

			boolJobCreated = createJOB(objBatchRequest);

            return boolJobCreated;
        }

        public void addBatchToJob(PayloadRequest objPayload) {
            foreach(List<string> lstLines in objPayload.BatchLines) {
                addBatchToJob (_objJobResponse, lstLines.ToArray ());
			}

            closeJob (_objJobResponse);
        }
    }
}

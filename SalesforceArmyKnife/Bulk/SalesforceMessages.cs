﻿//
//  SalesforceMessages.cs
//
//  Author:
//       Matías Rodríguez Llanos <mrllanos@gmail.com>
//
//  Copyright (c) 2017 Valmax IT S.A.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace valmaxit.salesforce.bulk {
    public static class SalesforceMessages {
        public sealed class RESTRequest {
			public enum HttpVerb {
				GET,
				POST,
				PUT,
				DELETE
			}

            public string EndPoint { get; set; }
            public HttpVerb Method { get; set; }
            public string PostData { get; set; }
            public string ContentType { get; set; }
			public string UrlParamters { get; set; }
			public Dictionary<string, string> Headers { get; set; }

			public RESTRequest() {
                this.Method = HttpVerb.GET;
                this.ContentType = "text/xml";
                this.Headers = new Dictionary<string, string>();
            }

            public override string ToString() {
                StringBuilder objSB = null;

                objSB = new StringBuilder();

                objSB.AppendLine("RESTRequest {");
                objSB.AppendFormat("EndPoint : {0}\n", this.EndPoint);
				objSB.AppendFormat("Method : {0}\n", this.Method);
				objSB.AppendFormat("PostData : {0}\n", this.PostData);
				objSB.AppendFormat("ContentType : {0}\n", this.ContentType);
				objSB.AppendFormat("UrlParamters : {0}\n", this.UrlParamters);

				objSB.AppendLine("Headers [");

				foreach(KeyValuePair<string, string> objKeyValue in this.Headers) {
                    objSB.AppendFormat("{0} : {1}\n", objKeyValue.Key, objKeyValue.Value);
				}

                objSB.AppendLine("]");

				objSB.AppendLine("}");
				return objSB.ToString();
            }
		}

        public sealed class JobRequest {
			public enum EConcurrency {
				Serial,
				Parallel
			}

			public string Operation { get; set; }
			public string sObject { get; set; }
			public string ContentType { get; set; }
			public EConcurrency Concurrency { get; set; }

			public string toJSON() {
				String strJSON = null;

                strJSON = String.Format("{{\"operation\" : \"{0}\",\"object\" : \"{1}\", \"contentType\" : \"{2}\"}}", this.Operation, this.sObject, this.ContentType);

				return strJSON;
			}

			public string toXML() {
				StringBuilder objRequest = null;

				objRequest = new StringBuilder();
				objRequest.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
				objRequest.Append("<jobInfo  xmlns='http://www.force.com/2009/06/asyncapi/dataload'>");
				objRequest.AppendFormat("<operation>{0}</operation>", this.Operation);
				objRequest.AppendFormat("<object>{0}</object>", this.sObject);
				objRequest.AppendFormat ("<concurrencyMode>{0}</concurrencyMode>", this.Concurrency);
				objRequest.AppendFormat("<contentType>{0}</contentType>", this.ContentType);
            	objRequest.Append("</jobInfo>");
				
				return objRequest.ToString();
			}			
		}

        public sealed class Credentials {
            public String Username { get; set; }

            public String Password { get; set; }

            public String Token { get; set; }

            public String ClientID { get; set; }

            public String ClientSecret { get; set; }

			public bool IsSandbox { get; set; }

			public bool isValid() {
                return !String.IsNullOrEmpty(this.Username) && !String.IsNullOrEmpty(this.Password) && !String.IsNullOrEmpty(this.ClientID) && !String.IsNullOrEmpty(this.ClientSecret);
            }

            public String toRequest() {				
				StringBuilder objRequest = null;

				objRequest = new StringBuilder();

				objRequest.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
				objRequest.Append("<env:Envelope xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\">");
				objRequest.Append("<env:Body>");
				objRequest.Append("<n1:login xmlns:n1=\"urn:partner.soap.sforce.com\">");
				objRequest.AppendFormat("<n1:username>{0}</n1:username>", this.Username);
                objRequest.AppendFormat("<n1:password>{0}</n1:password>", this.Password + this.Token);
				objRequest.Append("</n1:login>");
				objRequest.Append("</env:Body>");
				objRequest.Append("</env:Envelope>");

				return objRequest.ToString();			
            }
        }

		[XmlRoot(ElementName = "jobInfo", IsNullable =true)]
		public class JobResponse {
			[XmlElement(ElementName = "id")]
			public string id;

			[XmlElement(ElementName = "operation")]
			public string operation;

			[XmlElement(ElementName = "object")]
			public string sObject;

            [XmlElement(ElementName = "createdById")]
    		public string createdById;

			[XmlElement(ElementName = "createdDate")]
			public DateTime createdDate;

			[XmlElement(ElementName = "systemModstamp")]
			public DateTime systemModstamp;

			[XmlElement(ElementName = "state")]
			public string state;

			[XmlElement(ElementName = "concurrencyMode")]
			public string concurrencyMode;

			[XmlElement(ElementName = "contentType")]
			public string contentType;
                        
            [XmlElement(ElementName = "numberBatchesQueued")]
            public int numberBatchesQueued;

			[XmlElement(ElementName = "numberBatchesInProgress")]
			public int numberBatchesInProgress;

			[XmlElement(ElementName = "numberBatchesCompleted")]
			public int numberBatchesCompleted;

			[XmlElement(ElementName = "numberBatchesFailed")]
			public int numberBatchesFailed;

			[XmlElement(ElementName = "numberBatchesTotal")]
			public int numberBatchesTotal;

			[XmlElement(ElementName = "numberRecordsProcessed")]
			public int numberRecordsProcessed;

			[XmlElement(ElementName = "numberRetries")]
			public int numberRetries;

			[XmlElement(ElementName = "apiVersion")]
			public decimal apiVersion;

			[XmlElement(ElementName = "numberRecordsFailed")]
			public decimal numberRecordsFailed;

			[XmlElement(ElementName = "totalProcessingTime")]
			public decimal totalProcessingTime;

			[XmlElement(ElementName = "apiActiveProcessingTime")]
			public decimal apiActiveProcessingTime;

			[XmlElement(ElementName = "apexProcessingTime")]
			public decimal apexProcessingTime;

			public static JobResponse parseResponse(String strXML) {
				XmlDocument objXML = null;
				XmlSerializer objSerializer = null;
				StringReader objStringReader = null;
				JobResponse objJobResponse = null;

                strXML = strXML.Replace(" xmlns=\"http://www.force.com/2009/06/asyncapi/dataload\"", string.Empty);
				objXML = new XmlDocument();
				objXML.LoadXml(strXML);

				if (objXML != null) {
					objSerializer = new XmlSerializer(typeof(JobResponse));
                    objStringReader = new StringReader(objXML.OuterXml);

					objJobResponse = (JobResponse)objSerializer.Deserialize(objStringReader);
					objStringReader.Dispose();
				}

				return objJobResponse;
			}

			public string toCloseJobRequest () {
				StringBuilder objRequest = null;

				objRequest = new StringBuilder ();
				objRequest.AppendLine ("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
				objRequest.Append ("<jobInfo  xmlns='http://www.force.com/2009/06/asyncapi/dataload'>");
				objRequest.Append ("<state>Closed</state>");
				objRequest.Append ("</jobInfo>");

				return objRequest.ToString ();
			}
		}

		[XmlRoot(ElementName = "batchInfo", IsNullable = true)]
		public class BatchResponse {
			[XmlElement(ElementName = "id")]
			public string id;

			[XmlElement(ElementName = "jobId")]
			public string jobId;

			[XmlElement(ElementName = "state")]
			public string state;

			[XmlElement(ElementName = "createdDate")]
			public DateTime createdDate;

			[XmlElement(ElementName = "systemModstamp")]
			public DateTime systemModstamp;

			[XmlElement(ElementName = "numberRecordsProcessed")]
			public int numberRecordsProcessed;

			[XmlElement(ElementName = "numberRecordsFailed")]
			public int numberRecordsFailed;

			[XmlElement(ElementName = "totalProcessingTime")]
			public int totalProcessingTime;

			[XmlElement(ElementName = "apiActiveProcessingTime")]
			public int apiActiveProcessingTime;

			[XmlElement(ElementName = "apexProcessingTime")]
			public int apexProcessingTime;
            		
			public static BatchResponse parseResponse(String strXML) {
				XmlDocument objXML = null;
				XmlSerializer objSerializer = null;
				StringReader objStringReader = null;
				BatchResponse objJobResponse = null;

				strXML = strXML.Replace(" xmlns=\"http://www.force.com/2009/06/asyncapi/dataload\"", string.Empty);
				objXML = new XmlDocument();
				objXML.LoadXml(strXML);

				if (objXML != null) {
					objSerializer = new XmlSerializer(typeof(BatchResponse));
					objStringReader = new StringReader(objXML.OuterXml);

					objJobResponse = (BatchResponse)objSerializer.Deserialize(objStringReader);
					objStringReader.Dispose();
				}

				return objJobResponse;
			}
		}


		[XmlRoot(Namespace = "urn:partner.soap.sforce.com", ElementName = "result", IsNullable = true)]
        public class LoginResponse {
            [XmlElement(ElementName = "metadataServerUrl")]
            public string MetadataServerUrl;

            [XmlElement(ElementName = "passwordExpired")]
            public bool PasswordExpired;

            [XmlElement(ElementName = "sandbox")]
            public bool Sandbox;

            [XmlElement(ElementName = "serverUrl")]
            public string ServerUrl;

            [XmlElement(ElementName = "sessionId")]
            public string SessionId;

            [XmlElement(ElementName = "userId")]
            public string UserId;

            [XmlElement(ElementName = "userInfo")]
            public UserInfoResult UserInfo;

            public string apiURL {
                get {
                    return this.ServerUrl.Substring(0, this.ServerUrl.LastIndexOf('/'));
                }
            }

			public string InstanceUrl {
				get {
                    return this.ServerUrl.Substring(0, this.ServerUrl.IndexOf("/services/"));
				}
			}

            public static LoginResponse parseResponse(String strXML) {
                XmlDocument objXML = null;
                XmlNode objRootNode = null;
                XmlNamespaceManager objManager = null;
                XmlSerializer objSerializer = null;
                StringReader objStringReader = null;
                LoginResponse objLoginResponse = null;

                objXML = new XmlDocument();
                objXML.LoadXml(strXML);

                if (objXML != null) {
                    objManager = new XmlNamespaceManager(objXML.NameTable);
                    objManager.AddNamespace("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
                    objManager.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance/");
                    objManager.AddNamespace("ns", "urn:partner.soap.sforce.com");

                    objRootNode = objXML.SelectSingleNode("/soapenv:Envelope/soapenv:Body/ns:loginResponse", objManager);

                    objSerializer = new XmlSerializer(typeof(LoginResponse));
                    objStringReader = new StringReader(objRootNode.InnerXml);

                    objLoginResponse = (LoginResponse)objSerializer.Deserialize(objStringReader);
                    objStringReader.Dispose();
                }

                return objLoginResponse;
            }
        }

        [XmlRoot(Namespace = "urn:partner.soap.sforce.com", ElementName = "userInfo", IsNullable = true)]
        public class UserInfoResult {
            [XmlElement(ElementName = "accessibilityMode")]
            public bool AccessibilityMode;

            [XmlElement(ElementName = "currencySymbol")]
            public string CurrencySymbol;

            [XmlElement(ElementName = "orgAttachmentFileSizeLimit")]
            public int OrgAttachmentFileSizeLimit;

            [XmlElement(ElementName = "orgDefaultCurrencyIsoCode")]
            public string OrgDefaultCurrencyIsoCode;

            [XmlElement(ElementName = "orgDisallowHtmlAttachments")]
            public bool OrgDisallowHtmlAttachments;

            [XmlElement(ElementName = "orgHasPersonAccounts")]
            public bool OrgHasPersonAccounts;

            [XmlElement(ElementName = "organizationId")]
            public string OrganizationId;

            [XmlElement(ElementName = "organizationMultiCurrency")]
            public bool OrganizationMultiCurrency;

            [XmlElement(ElementName = "organizationName")]
            public string OrganizationName;

            [XmlElement(ElementName = "profileId")]
            public string ProfileId;

            [XmlElement(ElementName = "roleId")]
            public string RoleId;

            [XmlElement(ElementName = "sessionSecondsValid")]
            public int SessionSecondsValid;

            [XmlElement(ElementName = "userDefaultCurrencyIsoCode")]
            public string UserDefaultCurrencyIsoCode;

            [XmlElement(ElementName = "userEmail")]
            public string UserEmail;

            [XmlElement(ElementName = "userFullName")]
            public string UserFullName;

            [XmlElement(ElementName = "userId")]
            public string UserId;

            [XmlElement(ElementName = "userLanguage")]
            public string UserLanguage;

            [XmlElement(ElementName = "userLocale")]
            public string UserLocale;

            [XmlElement(ElementName = "userName")]
            public string UserName;

            [XmlElement(ElementName = "userTimeZone")]
            public string UserTimeZone;

            [XmlElement(ElementName = "userType")]
            public string UserType;

            [XmlElement(ElementName = "userUiSkin")]
            public string UserUiSkin;
        }
    }
}

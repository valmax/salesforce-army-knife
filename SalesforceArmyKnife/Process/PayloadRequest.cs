﻿//
//  PayloadRequest.cs
//
//  Author:
//       Matías Rodríguez Llanos <mrllanos@gmail.com>
//
//  Copyright (c) 2017 Valmax IT S.A.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace valmaxit.salesforce.process {
	public sealed class PayloadRequest {
        private int ARRAY_LEN = int.Parse(ConfigurationManager.AppSettings["batchSize"]);
        public List<List<string>> BatchLines = null;

        public PayloadRequest (string strFile) {
            initializeClass (strFile);
        }

        private void initializeClass(string strFile) {
            string[] arrLines = null;
            int intChunks = 0;
            string strHeaderLine = null;
            List<string> lstBatch = null;

            if(File.Exists (strFile)) {
                arrLines = File.ReadAllLines (strFile);
                /* 
                for(int intIndex = 1; intIndex < arrLines.Length; intIndex++) {
                    arrLines[intIndex] = arrLines[intIndex].Split(',')[0] + ",\"#N/A\"";
                }
                */
                intChunks = (arrLines.Length - 1) / ARRAY_LEN;

                if((arrLines.Length - 1) % ARRAY_LEN > 0) {
                    intChunks++;
                }

                Console.WriteLine ("Cantidad de Chunks : " + intChunks);
                BatchLines = new List<List<string>> ();
                strHeaderLine = arrLines [0]; 
                for (int intIndex = 0; intIndex < intChunks; intIndex++) {            
                    lstBatch = arrLines.Skip (intIndex * ARRAY_LEN).Take (ARRAY_LEN).ToList ();

                    if (intIndex > 0) {
						lstBatch.Insert (0, strHeaderLine);
					}

					BatchLines.Add (lstBatch);
                }                               
			} else {
                throw new FileNotFoundException ();
            }
        }
	}
}
